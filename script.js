/* Internet Explorer does not support arrow functions */

var max = parseInt($('#range_max').text());
var min = parseInt($('#range_min').text());
var area = $('#area');
var price = $('#price');
var stuffDesc = $('#stuff-desc');

$('.stuff-image').hover(function() {
    stuffDesc.show();
    if (window.matchMedia('(max-device-width: 768px)').matches) {
        switch (e.currentTarget.getAttribute('id')) {
            case 'flat':
                stuffDesc[0].style.top = '0%';
                break;
            case 'garage':
                stuffDesc[0].style.top = '25%';
                break;
            case 'office':
                stuffDesc[0].style.top = '50%';
                break;
            case 'lab':
                stuffDesc[0].style.top = '75%';
                break;
            default:
                break;
        }
    } else {
        switch (e.currentTarget.getAttribute('id')) {
            case 'flat':
                stuffDesc[0].style.left = '0%';
                break;
            case 'garage':
                stuffDesc[0].style.left = '25%';
                break;
            case 'office':
                stuffDesc[0].style.left = '50%';
                break;
            case 'lab':
                stuffDesc[0].style.left = '75%';
                break;
            default:
                break;
        }
    }
});

stuffDesc.hover(() => {
    console.log(stuffDesc);
    stuffDesc.show();
}, () => {
    stuffDesc.hide();
});

function Slider(input, level, output, area, price) {
    this.input = input;
    this.level = level;
    this.output = output;
    input[0].addEventListener('input', function() {
        this.updateSliderOutput();
        this.updateSliderLevel();
    }.bind(this), false);

    this.getValue = function() {
        return this.input.val();
    }

    this.levelString = function() {
        return parseInt(this.getValue());
    }

    this.levelPercent = function() {
        return this.levelString() / (max + min) * 100;
    }

    this.levelPixel = function() {
        return this.levelString() * (this.input.width() - 15) / (max - min) - 7;
    }

    this.updateSliderOutput = function() {
        console.log(min);
        area.text(this.levelString() + " м²");
        price.text(this.levelString() * 600 + " ₽");
        output[0].value = this.levelString();
        output[0].style.left = this.levelPercent() - (max + min) / 100 - this.output.width() / 6 + '%';
    }

    this.updateSlider = function(num) {
        this.input.val(num);
        this.updateSliderLevel();
        this.updateSliderOutput();
    }

    this.updateSliderLevel = function() {
        let sliderWidth = this.input.width();
        console.log(this.levelPercent());
        this.level.css('width', this.levelPixel() + 'px');
    }
}

$(document).ready(() => {
    $('.blank').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    $('.examples .columns').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                width: "100px"
            }
        }]
    });

    stuffDesc.hide();
    var slider = new Slider($('.volume .slider-input'), $('.volume .slider-level'), $('.volume .slider-output'), area, price);
    slider.updateSliderOutput();
    slider.updateSliderLevel();
    $('#range .total').keypress((e) => {
        let max = parseInt($('#range-max').text());
        if (e.keyCode != 13 && (e.keyCode < 48 || e.keyCode > 57)) {
            return false;
        }
        let current = $('#range .total').val();
        if (current === '') {
            current = min;
        }
        if (e.keyCode == 13) {
            $('#range .total').val('');
            slider.updateSlider(current);
        }
        if (current.toString().length < max.toString().length) {
            return true;
        }
        return false;
    });

});